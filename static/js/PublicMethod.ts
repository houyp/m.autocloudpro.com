/*
 *  项目的公用方法集合
 *  类似但区别于vuex
 *  区别在于：不涉及应用状态数据，仅作为封装处理使用
 */

const method = {
  // TEST:点击弹出，测试方法，组件内调用方法：this.PublicMethod.click()
  click () {
    alert('测试：点击弹出')
  }
}
export default method
