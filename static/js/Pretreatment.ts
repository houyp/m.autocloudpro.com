// 预处理文件
// 加载于应用入口
// houyunpeng
// 2018-01-08 13:41:51
import Axios from 'axios'

// Number.prototype.toFixed = (fractionDigits: number) => {
//   // 没有对fractionDigits做任何处理，假设它是合法输入
//   return (parseInt(this * Math.pow(10, fractionDigits) + 0.5) / Math.pow(10, fractionDigits)).toString()
// }

// 修改axios请求方式，默认传参格式
Axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'
Axios.defaults.transformRequest = [ function (data) {
  let newData = ''
  for (let k in data) {
    newData += encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) + '&'
  }
  newData = newData.substring(0, newData.length - 1)
  // console.log('newData', JSON.stringify(newData))
  return newData
}]
Axios.defaults.transformResponse = [ function (data) {
  // console.log('data', JSON.stringify(data))
  try {
    let datas = typeof data === 'string' ? JSON.parse(data) : data
    if (datas.code === 17) {
      sessionStorage.setItem('isLogin', 'false')
      console.log(location.protocol + location.host + '/#/login')
      window.location.reload()
    }
    return datas
  } catch (err) {
    return data
  }
}]

// 设备判断
if (navigator.userAgent.match(/(nokia|iphone|android|ipad|motorola|softbank|foma|docomo|kddi|up\.browser|up\.link|htc|dopod|blazer|netfront|helio|hosin|huawei|novarra|CoolPad|webos|techfaith|palmsource|blackberry|alcatel|amoi|ktouch|nexian|samsung|s[cg]h|^lge|ericsson|philips|sagem|wellcom|bunjalloo|maui|symbian|smartphone|midp|wap|phone|windows ce|iemobile|^spice|^bird|longcos|pantech|gionee|portalmmm|jig\s browser|hiptop|^ucweb|^benq|haier|^lct|opera\s*mobi|opera\*mini|320x320|240x320|176x220)/i)) {
  let targetProtocol = 'https:'
  if (window.location.protocol !== targetProtocol && (window.location.host === 'm.autocloudpro.com' || window.location.host === 'm.tecepc.com')) {
    window.location.href = targetProtocol + window.location.href.substring(window.location.protocol.length)
  }
} else {
  window.location.href = 'http://www.autocloudpro.com/'
}

// serviceworker
if (window.location.protocol === 'https:') {
  if ('serviceWorker' in navigator) {
    // register service worker
    navigator.serviceWorker
      .register('/service-worker.js')
      .then(function () { console.log('Service Worker Registered') })
  }
}

// 移动端300毫秒BUG
window.onload = function () {
  document.addEventListener('gesturestart', function (e) {
    e.preventDefault()
  })
  document.addEventListener('dblclick', function (e) {
    e.preventDefault()
  })
  document.addEventListener('touchstart', function (event) {
    if (event.touches.length > 1) {
      event.preventDefault()
    }
  })
  let lastTouchEnd = 0
  document.addEventListener('touchend', function (event) {
    let now = (new Date()).getTime()
    if (now - lastTouchEnd <= 300) {
      event.preventDefault()
    }
    lastTouchEnd = now
  }, false)
}
