// This file is intentionally without code.
// It's present so that service worker registration
// will work when serving from the 'public' directory.
// name: chanxin
// wechat: donkeith

// // 定义名称，后期更新该参数，达到更新缓存的目的
// const CACHE_NAME = 'autocloudproV2.0'
//
// // 在app被安装时触发。它经常用来缓存必要的文件。缓存通过 Cache API来实现。
// this.addEventListener('install', function (event) {
//   this.skipWaiting()
//   console.log('install service worker')
//   // 创建和打开一个缓存库
//   caches.open(CACHE_NAME)
//   // 首页
//   let cacheResources = ['https://m.autocloudpro.com/']
//   event.waitUntil(
//     // 请求资源并添加到缓存里面去
//     caches.open(CACHE_NAME).then(cache => {
//       cache.addAll(cacheResources)
//     })
//   )
// })
//
// // 当 install完成后， service worker 进入active状态，这个事件立刻执行
// this.addEventListener('activate', function (event) {
//   console.log('service worker is active')
//   // delete old caches
//   event.waitUntil(
//     clearOldCaches()
//       .then(() => self.clients.claim())
//   )
// })
//
// // 当有网络请求时这个事件被触发。它调用respondWith()方法来劫持 GET 请求并返回
// this.addEventListener('fetch', function (event) {
//   event.respondWith(
//     caches.match(event.request, {ignoreVary: true}).then(response => {
//       // cache hit
//       if (response) {
//         return response
//       }
//       return util.fetchPut(event.request.clone())
//     })
//   )
// })
//
// let util = {
//   fetchPut: function (request, callback) {
//     return fetch(request).then(response => {
//       // 跨域的资源直接return
//       if (!response || response.status !== 200 || response.type !== 'basic') {
//         return response
//       }
//       util.putCache(request, response.clone())
//       typeof callback === 'function' && callback()
//       return response
//     })
//   },
//   putCache: function (request, resource) {
//     // 后台不要缓存，preview链接也不要缓存
//     if (request.method === 'GET' && request.url.indexOf('wp-admin') < 0 && request.url.indexOf('preview_id') < 0) {
//       caches.open(CACHE_NAME).then(cache => {
//         cache.put(request, resource)
//       })
//     }
//   }
// }
//
// // 清除旧的缓存，通过CACHE_NAME区别
// function clearOldCaches () {
//   return caches.keys()
//     .then(keylist => {
//       return Promise.all(
//         keylist
//           .filter(key => key !== CACHE_NAME)
//           .map(key => caches.delete(key))
//       )
//     })
// }
