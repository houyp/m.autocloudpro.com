import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import Home from '@/components/index.vue'
// 登录模块
import Login from '@/components/login/login.vue'
import Register from '@/components/login/register.vue'
import forgetPassword from '@/components/login/forget_password.vue'
// 购物车模块
import Cart from '@/components/cart/cart.vue'
import Purchase from '@/components/cart/purchase.vue'
// 订单模块
import Order from '@/components/order/order_list.vue'
import orderDetail from '@/components/order/order_detail.vue'
// 个人中心模块
import Center from '@/components/center/center.vue'
// 云豆充值
import Renewals from '@/components/center/renewals.vue'
// 返现模块
import cashback from '@/components/cashback/cashback.vue'
import cashbackDetail from '@/components/cashback/cashback_detail.vue'
// 白条模块
import white from '@/components/white/white.vue'
import whiteBill from '@/components/white/white_bill.vue'
import whiteDetail from '@/components/white/white_detail.vue'
// 支付密码模块
import PayPassword from '@/components/paypassword/pay_password.vue'
// 业务流模块
import modelList from '@/components/inquire/model_list.vue'
import modelQueries from '@/components/inquire/model_queries.vue'
import partsGroup from '@/components/inquire/parts_group.vue'
import assemblyDiagram from '@/components/inquire/assembly_diagram.vue'
import partsList from '@/components/inquire/parts_list.vue'
import partsDetails from '@/components/inquire/parts_details.vue'
import rapidInquiry from '@/components/inquire/rapid_inquiry.vue'
import rapidInquiryHistory from '@/components/inquire/rapid_inquiry_history.vue'
// 地址管理模块
import addressList from '@/components/address/address_list.vue'
import addressEdit from '@/components/address/address_edit.vue'
// 售后模块
import aftersaleApply from '@/components/aftersale/aftersale_apply.vue'
import aftersaleDetail from '@/components/aftersale/aftersale_detail.vue'
// 公共模块
import Feedback from '@/components/publics/_feedback.vue'
import FeedbackAfterSale from '@/components/publics/_feedback.aftersale.vue'
import Article from '@/components/publics/_article.vue'
import Pay from '@/components/publics/_pay.vue'
import nofind from '@/components/publics/_nofind.vue'
import Logoff from '@/components/publics/_Logoff.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/nofind',
      name: 'nofind',
      component: nofind
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/forgetPassword',
      name: 'forgetPassword',
      component: forgetPassword
    },
    {
      path: '/cart',
      name: 'cart',
      component: Cart
    },
    {
      path: '/cart/purchase',
      name: 'purchase',
      component: Purchase
    },
    {
      path: '/center/addressList',
      name: 'addressList',
      component: addressList
    },
    {
      path: '/center/addressList/addressEdit',
      name: 'addressEdit',
      component: addressEdit
    },
    {
      path: '/order',
      name: 'order',
      component: Order
    },
    {
      path: '/order/orderDetail',
      name: 'orderDetail',
      component: orderDetail
    },
    {
      path: '/center',
      name: 'center',
      component: Center
    },
    {
      path: '/center/cashback',
      name: 'cashback',
      component: cashback
    },
    {
      path: '/center/cashback/cashbackDetail',
      name: 'cashbackDetail',
      component: cashbackDetail
    },
    {
      path: '/center/white',
      name: 'white',
      component: white
    },
    {
      path: '/center/white/whiteBill',
      name: 'whiteBill',
      component: whiteBill
    },
    {
      path: '/center/white/whiteBill/whiteDetail',
      name: 'whiteDetail',
      component: whiteDetail
    },
    {
      path: '/center/renewals',
      name: 'renewals',
      component: Renewals
    },
    {
      path: '/center/paypassword',
      name: 'paypassword',
      component: PayPassword
    },
    {
      path: '/modelList',
      name: 'modelList',
      component: modelList
    },
    {
      path: '/modelQueries',
      name: 'modelQueries',
      component: modelQueries
    },
    {
      path: '/rapidInquiry',
      name: 'rapidInquiry',
      component: rapidInquiry
    },
    {
      path: '/rapidInquiry/rapidInquiryHistory',
      name: 'rapidInquiryHistory',
      component: rapidInquiryHistory
    },
    {
      path: '/modelList/partsGroup',
      name: 'partsGroup',
      component: partsGroup
    },
    {
      path: '/modelList/partsGroup/assemblyDiagram',
      name: 'assemblyDiagram',
      component: assemblyDiagram
    },
    {
      path: '/modelList/partsGroup/assemblyDiagram/partsList',
      name: 'partsList',
      component: partsList
    },
    {
      path: '/modelList/partsGroup/assemblyDiagram/partsList/partsDetails',
      name: 'partsDetails',
      component: partsDetails
    },
    {
      path: '/feedback',
      name: 'feedback',
      component: Feedback
    },
    {
      path: '/feedbackaftersale',
      name: 'feedbackaftersale',
      component: FeedbackAfterSale
    },
    {
      path: '/article',
      name: 'article',
      component: Article
    },
    {
      path: '/pay',
      name: 'pay',
      component: Pay
    },
    {
      path: '/aftersaleApply',
      name: 'aftersaleApply',
      component: aftersaleApply
    },
    {
      path: '/aftersaleApply/aftersaleDetail',
      name: 'aftersaleDetail',
      component: aftersaleDetail
    },
    {
      path: '/logoff',
      name: 'logoff',
      component: Logoff
    }
  ]
})

router.beforeEach((to, from, next) => {
  store.commit('HIDE_NAVSHOW')
  if (to.path !== '/logoff') {
    next('/logoff')
  } else {
    next()
  }
  // // 更新登录状态
  // store.commit('CHANGE_LOG', sessionStorage.getItem('isLogin') === 'true')

  // // 底部导航显示控制
  // if (to.name === 'login' || to.name === 'register' || to.name === 'forgetPassword' || to.name === 'pay') {
  //   store.commit('HIDE_NAVSHOW')
  // } else {
  //   store.commit('OPEN_NAVSHOW')
  // }

  // // 页面刷新
  // if (to.name === 'pay' && from.name !== null) {
  //   setTimeout(() => {
  //     window.location.reload()
  //   }, 0)
  //   next()
  // }

  // // 未登录跳转
  // if (!store.state.userMessages.isLogin && to.name !== 'login' && to.name !== 'register' && to.name !== 'forgetPassword') {
  //   if (from.name === 'login') {
  //     store.commit('HIDE_NAVSHOW')
  //     return false
  //   } else {
  //     next('/login')
  //   }
  // }
  // if (to.matched.length === 0) {
  //   if (from.name) {
  //     next({ name: from.name })
  //   } else {
  //     next('/')
  //   }
  // } else {
  //   next()
  // }
})

export default router
