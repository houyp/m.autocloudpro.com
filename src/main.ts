// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Axios from 'axios'
import MuseUI from 'muse-ui'
import BottomMenu from './components/publics/_bottommenu.vue'
import Datepicker from './components/publics/_datepicker.vue'
import Order from './components/order/order.vue'
import 'muse-ui/dist/muse-ui.css'
import '../static/css/iconfont.css'
import '../static/css/Pretreatment.css'
import '../static/js/Pretreatment'
import PublicMethod from '../static/js/PublicMethod'
import * as filters from './filters'

/* Vue配置，注册公用组件，公共过滤器，自定义命名 */
Vue.config.productionTip = false
Vue.component('BottomMenu', BottomMenu)
Vue.component('Datepicker', Datepicker)
Vue.component('Order', Order)

Object.keys(filters).forEach((key: string) => {
  let objectSource: any = filters
  Vue.filter(key, objectSource[key])
})

Vue.directive('focus', {
  update: function (el, {value}) {
    if (value) {
      el.focus()
    }
  }
})

/* 挂载请求库Axios,公用方法 */
Vue.use(MuseUI)
Vue.prototype.Axios = Axios
Vue.prototype.PublicMethod = PublicMethod

/* tslint:disable:no-unused-expression */
const app: Vue = new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
/* tslint:enable:no-unused-expression */
export default app
