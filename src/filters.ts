/**
 * 过滤器
 * @param value
 */

// 超过3位省略
export function OmittedThree (value: string): string {
  if (!value) return ''
  let values: string
  values = value.toString()
  if (values.length > 3) {
    value = value.substring(0, 3) + '..'
  }
  return value
}

// 超过4位省略
export function OmittedFour (value: string): string {
  if (!value) return ''
  let values: string
  values = value.toString()
  if (values.length > 4) {
    value = value.substring(0, 4) + '..'
  }
  return value
}

// 超过10位省略
export function OmittedTen (value: string): string {
  if (!value) return ''
  let values: string
  values = value.toString()
  if (values.length > 10) {
    value = value.substring(0, 10) + '..'
  }
  return value
}

// 超过15位省略
export function OmittedFifteen (value: string): string {
  if (!value) return ''
  let values: string
  values = value.toString()
  if (values.length > 15) {
    value = value.substring(0, 15) + '..'
  }
  return value
}

// 超过32位省略
export function OmittedThirtyTwo (value: string): string {
  if (!value) return ''
  let values: string
  values = value.toString()
  if (values.length > 32) {
    value = value.substring(0, 32) + '..'
  }
  return value
}

// 超过22位省略
export function OmittedTwentyTwo (value: string): string {
  if (!value) return ''
  let values: string
  values = value.toString()
  if (values.length > 22) {
    value = value.substring(0, 22) + '..'
  }
  return value
}

// 处理为价格形式
export function Price (value: string): string {
  if (!value) return '0.00'
  let valueAfter: string = parseFloat(value).toFixed(2)
  return valueAfter
}

// 字符替换
export function CharacterReplacement (value: string): string {
  if (!value) return ''
  let a = value.replace(/x/i, '1')
  let b = a.replace(/n/i, '1')
  return b
}

// 过滤数据中的'9999/99999/999999'为'至今'
export function ToDate (value: string): string {
  if (!value) return ''
  value = value.toString()
  let b = value.replace(/999999/, '至今')
  let c = b.replace(/99999/, '至今')
  let d = c.replace(/9999/, '至今')
  return d
}

// 过滤为星期
export function week (item: number, lang: string): any {
  interface ObjType {
    [key: number]: string
  }
  switch (lang) {
    case 'en':
      let Objen: ObjType = {
        '0': 'Su',
        '1': 'Mo',
        '2': 'Tu',
        '3': 'We',
        '4': 'Th',
        '5': 'Fr',
        '6': 'Sa'
      }
      return Objen[item]
    case 'ch':
      let Objch: ObjType = {
        '0': '日',
        '1': '一',
        '2': '二',
        '3': '三',
        '4': '四',
        '5': '五',
        '6': '六'
      }
      return Objch[item]
    default:
      return item
  }
}

// 过滤为月份
export function month (item: number, lang: string): any {
  interface ObjType {
    [key: number]: string
  }
  switch (lang) {
    case 'en':
      let Objen: ObjType = {
        1: 'Jan',
        2: 'Feb',
        3: 'Mar',
        4: 'Apr',
        5: 'May',
        6: 'Jun',
        7: 'Jul',
        8: 'Aug',
        9: 'Sep',
        10: 'Oct',
        11: 'Nov',
        12: 'Dec'
      }
      return Objen[item]
    case 'ch':
      let Objch: ObjType = {
        1: '1月',
        2: '2月',
        3: '3月',
        4: '4月',
        5: '5月',
        6: '6月',
        7: '7月',
        8: '8月',
        9: '9月',
        10: '10月',
        11: '11月',
        12: '12月'
      }
      return Objch[item]
    default:
      return item
  }
}
