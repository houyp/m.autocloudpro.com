declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}

declare module 'muse-ui'

declare module 'lrz'

declare module 'qrcode'

// declare module 'vuex-persistedstate'

declare function unescape (str: string): string
