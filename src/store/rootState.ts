/*
 *    状态树
 *    默认数据
 */
const state = {
  userMessages: {                     // 用户信息
    isLogin: false,                   // 登录状态   ture登录  false未登录
    Avatar: '',                       // 头像
    userName: '',                     // 用户名
    cashback: {
      cashbackBalance: 0              // 返现余额
    },
    whitestripes: {
      whitestripesState: 'close',     // close:关闭  open:开启  freeze:冻结
      whitestripesBalance: 0          // 白条余额
    },
    cloudBeans: {
      cloudBeansBalance: 0            // 云豆余量
    }
  },
  nav: {
    show: true                        // 导航栏目状态
  },
  alert: {
    alertshow: false,                 // 弹出提示框
    alertText: ''                     // 提示框文案
  },
  linearshow: false                   // 进度条
}

export default state
