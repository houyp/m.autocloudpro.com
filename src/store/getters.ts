/*
 *    获取state的数据，单向，读取
 */
const getters = {
  navShow: (state: any) => state.navShow,
  isLogin: (state: any) => state.userMessages.isLogin,
  Avatar: (state: any) => state.userMessages.Avatar,
  userName: (state: any) => state.userMessages.userName,
  cashbackBalance: (state: any) => (state.userMessages.cashback.cashbackBalance - 0).toFixed(2),
  whitestripesState: (state: any) => state.userMessages.whitestripes.whitestripesState,
  whitestripesBalance: (state: any) => (state.userMessages.whitestripes.whitestripesBalance - 0).toFixed(2),
  cloudBeansBalance: (state: any) => state.userMessages.cloudBeans.cloudBeansBalance
}

export default getters
