/*
 *    定义state数据的修改操作
 */
const mutations = {
  // 登录状态变更
  CHANGE_LOG (state: any, val: boolean) {
    state.userMessages.isLogin = val
  },
  // 显示底部导航
  OPEN_NAVSHOW (state: any) {
    state.nav.show = true
  },
  // 隐藏底部导航
  HIDE_NAVSHOW (state: any) {
    state.nav.show = false
  },
  // 更新用户信息
  UPDATEUSERMESSGAES (state: any, Obj: any) {
    state.userMessages.Avatar = Obj.Avatar
    state.userMessages.userName = Obj.userName
    state.userMessages.cashback.cashbackBalance = Obj.cashback.cashbackBalance
    state.userMessages.whitestripes.whitestripesState = Obj.whitestripes.whitestripesState
    state.userMessages.whitestripes.whitestripesBalance = Obj.whitestripes.whitestripesBalance
    state.userMessages.cloudBeans.cloudBeansBalance = Obj.cloudBeans.cloudBeansBalance
  },
  // 控制弹出框显示
  OPEN_ALERT (state: any, obj: any) {
    state.alert.alertshow = true
    state.alert.alertText = obj.val
    setTimeout(() => {
      state.alert.alertshow = false
      state.alert.alertText = ''
    }, obj.wait)
  },
  // 控制进度条显示
  CHANGE_LINEAR (state: any, bool: boolean) {
    state.linearshow = bool
  }
}

export default mutations
